import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}  String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String ThaiGreeting = "สวัสดี Flutter";
String LatinGreeting = "Salve Flutter";

class _HelloFlutterAppState extends State<HelloFlutterApp> {


  String displayText = englishGreeting;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
              onPressed: () {
                setState(() {
                  displayText = displayText == englishGreeting?
                  spanishGreeting:englishGreeting;
                });
              },
              icon: Icon(Icons.refresh),
            ),
            IconButton(
              onPressed: () {
                setState(() {
                  displayText = displayText == englishGreeting?
                  ThaiGreeting:englishGreeting;
                });
              },
              icon: Icon(Icons.abc),
            ),
            IconButton(
              onPressed: () {
                setState(() {
                  displayText = displayText == englishGreeting?
                  LatinGreeting :englishGreeting;
                });
              },
              icon: Icon(Icons.bedtime),
            ),
          ],
        ),
        body: Center(
          child: Text(
              displayText,
              style: TextStyle(fontSize: 24)
          ),
        ),
      ),
    );
  }
}
/*
class helloflutterApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
<<<<<<< HEAD
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {},
                icon: Icon(Icons.refresh))
          ],
        ),
        body: Center(
          child: Text(
              "Hello Flutter ",
              style: TextStyle(fontSize: 24)
          ),
=======
  return MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Scaffold(
      appBar: AppBar(
        title: Text("Hello Flutter"),
        leading: Icon(Icons.home),
        actions: <Widget>[
          IconButton(
              onPressed: () {},
              icon: Icon(Icons.refresh))
        ],
      ),
      body: Center(
        child: Text(
          "Hello Flutter ",
        style: TextStyle(fontSize: 24)
        ),
>>>>>>> a470196cdd5c1a3c2c6b007be675b52666693750
        ),
      ),
    );
  }
}
*/